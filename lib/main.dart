import 'package:flutter/material.dart';
import 'package:layouts/pages/tabs.page.dart';
import 'package:layouts/themes/light.theme.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: lightTheme(),
      home: DefaultTabController(length: 3, child: TabPage()),
    );
  }

  //comando para gerar icon
  //flutter pub run flutter_launcher_icons:main
}
